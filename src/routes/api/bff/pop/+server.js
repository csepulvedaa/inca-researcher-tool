// Import the json utility from '@sveltejs/kit' for sending JSON responses
import { json } from '@sveltejs/kit';
import { INCA_APPS } from '$lib/constants.js';

const POP_END = INCA_APPS.POP_END;
const POP = INCA_APPS.POP;

// Adjusted to accept the `fetch` parameter for server-side fetching
async function fetchAccuracy(fetch, selectedSubject, uid, startDate, endDate) {

    const url = `/api/statistics/accuracy/pop?firestoreCollection=${POP_END}&subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`;
    const responseFetch = await fetch(url);

    if (!responseFetch.ok) {
        console.log(`Response Status: ${responseFetch.status}`);
        try {
            const responseBody = await responseFetch.text(); // Attempt to read the body even in case of failure
            console.log(`Response Body: ${responseBody}`);
        } catch (bodyError) {
            console.log(`Error reading response body: ${bodyError}`);
        }

        throw new Error(`Fetch to Accuracy failed with status: ${responseFetch.status}`);
    }

    return responseFetch.json();
}

// Adjusted to accept the `fetch` parameter for server-side fetching
async function fetchContingencyTable(fetch, selectedSubject, uid, startDate, endDate) {
    const url = `/api/statistics/contingency-table/pop?firestoreCollection=${POP}&subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`;
    const responseFetch = await fetch(url);

    if (!responseFetch.ok) {
        console.log(`Response Status: ${responseFetch.status}`);
        try {
            const responseBody = await responseFetch.text(); // Attempt to read the body even in case of failure
            console.log(`Response Body: ${responseBody}`);
        } catch (bodyError) {
            console.log(`Error reading response body: ${bodyError}`);
        }

        throw new Error(`Fetch to Contingency Table failed with status: ${responseFetch.status}`);
    }

    return responseFetch.json();
}

async function fetchChiSquaredColors(fetch, contingencyTable) {
    const chiSquaredColorsResponse = await fetch("/api/statistics/chi-squared/contingency/pop", {
        method: "POST",
        body: JSON.stringify(contingencyTable),
        headers: { "Content-Type": "application/json" },
    });
    if (chiSquaredColorsResponse.ok) {
        return chiSquaredColorsResponse.json();
    } else {
        console.error("Respuesta no exitosa: Chi Squared Colors", chiSquaredColorsResponse.statusText);
        return {};
    }
}

async function fetchPoppedAndTotal(fetch, selectedSubject, uid, startDate, endDate) {
    const url = `/api/statistics/value-counts/pop?subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`;
    const responseFetch = await fetch(url);

    if (!responseFetch.ok) {
        console.log(`Response Status: ${responseFetch.status}`);
        try {
            const responseBody = await responseFetch.text(); // Attempt to read the body even in case of failure
            console.log(`Response Body: ${responseBody}`);
        } catch (bodyError) {
            console.log(`Error reading response body: ${bodyError}`);
        }

        throw new Error(`Fetch to Popped And Total failed with status: ${responseFetch.status}`);
    }

    return responseFetch.json();
}

async function fetchScreenTaps(fetch, selectedSubject, uid, startDate, endDate) {
    const url = `/api/statistics/value-counts/pop/screen-taps?subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`;
    const responseFetch = await fetch(url);

    if (!responseFetch.ok) {
        console.log(`Response Status: ${responseFetch.status}`);
        try {
            const responseBody = await responseFetch.text(); // Attempt to read the body even in case of failure
            console.log(`Response Body: ${responseBody}`);
        } catch (bodyError) {
            console.log(`Error reading response body: ${bodyError}`);
        }

        throw new Error(`Fetch to Screen Taps failed with status: ${responseFetch.status}`);
    }

    return responseFetch.json();
}

// The GET function using the provided `fetch` for server-side fetching
export async function GET({ url, fetch }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? undefined);
    const endDate = String(url.searchParams.get('endDate') ?? undefined);
    try {
        const accuracy = await fetchAccuracy(fetch, selectedSubjectType, uid, startDate, endDate);
        const contingencyTable = await fetchContingencyTable(fetch, selectedSubjectType, uid, startDate, endDate);
        const chiSquaredColors = await fetchChiSquaredColors(fetch, contingencyTable);
        const poppedAndTotal = await fetchPoppedAndTotal(fetch, selectedSubjectType, uid, startDate, endDate);
        const screenTaps = await fetchScreenTaps(fetch, selectedSubjectType, uid, startDate, endDate);
        // Packaging the response data

        const response = {
            accuracy,
            contingencyTable: contingencyTable.contingencyTable,
            chiSquaredColors,
            poppedAndTotal,
            screenTaps,
        };
        // Returning the combined data as a JSON response
        return json(response);
    } catch (error) {
        console.error(error);
        // Returning an error response if any of the fetch operations fail
        return json({ error: 'An error occurred while fetching data.' }, { status: 500 });
    }
}
