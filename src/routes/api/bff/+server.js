// Import the json utility from '@sveltejs/kit' for sending JSON responses
import { json } from '@sveltejs/kit';

// Adjusted to accept the `fetch` parameter for server-side fetching
async function fetchValueCounts(fetch, selectedSubject, uid , firestoreCollection, startDate, endDate) {
    const url = `/api/statistics/value-counts?firestoreCollection=${firestoreCollection}&subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`;
    const responseFetch = await fetch(url);
    
    if (!responseFetch.ok) {
        console.log(`Response Status: ${responseFetch.status}`);
        try {
            const responseBody = await responseFetch.text(); // Attempt to read the body even in case of failure
            console.log(`Response Body: ${responseBody}`);
        } catch (bodyError) {
            console.log(`Error reading response body: ${bodyError}`);
        }
        
        throw new Error(`Fetch to Value Counts failed with status: ${responseFetch.status}`);
    }
    
    return responseFetch.json();
}


async function fetchBinominalTest(fetch, valueCounts) {
    const binominalTestResponse = await fetch("/api/statistics/binomial-test", {
        method: "POST",
        body: JSON.stringify(valueCounts),
        headers: { "Content-Type": "application/json" },
    });
    if (binominalTestResponse.ok) {
        return binominalTestResponse.json();
    } else {
        console.error("Respuesta no exitosa: Binominal Test", binominalTestResponse.statusText);
        throw new Error("Fetch to Binominal Test failed");
    }
}

async function fetchContingencyTable(fetch, selectedSubject, uid , firestoreCollection, startDate, endDate) {
    const responseFetch = await fetch(`/api/statistics/contingency-table?firestoreCollection=${firestoreCollection}&subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`);
    if (responseFetch.ok) {
        return responseFetch.json();
    } else {
        console.error("Respuesta no exitosa Contingency Table:", responseFetch.statusText);
        throw new Error("Fetch to Contingency Table failed");
    }
}

async function fetchChiSquaredSideBias(fetch, contingencyTable) {
    const chiSquaredSideBiasResponse = await fetch("/api/statistics/chi-squared/contingency", {
        method: "POST",
        body: JSON.stringify(contingencyTable),
        headers: { "Content-Type": "application/json" },
    });
    if (chiSquaredSideBiasResponse.ok) {
        return chiSquaredSideBiasResponse.json();
    } else {
        console.error("Respuesta no exitosa: Chi Squared Side Bias", chiSquaredSideBiasResponse.statusText);
        return {};
    }
}

async function fetchChiSquaredPearson(fetch, contingencyTable) {
    const chiSquaredSideBiasResponse = await fetch("/api/statistics/chi-squared", {
        method: "POST",
        body: JSON.stringify(contingencyTable),
        headers: { "Content-Type": "application/json" },
    });
    if (chiSquaredSideBiasResponse.ok) {
        return chiSquaredSideBiasResponse.json();
    } else {
        console.error("Respuesta no exitosa: Chi Squared Side Bias", chiSquaredSideBiasResponse.statusText);
        return {};
    }
}

async function fetchCorrelationValues(fetch, selectedSubject, uid, firestoreCollection, startDate, endDate) {
    const responseFetch = await fetch(`/api/statistics/correlation/values?firestoreCollection=${firestoreCollection}&subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`);
    if (responseFetch.ok) {
        return responseFetch.json();
    } else {
        console.error("Respuesta no exitosa: Correlation Test", responseFetch.statusText);
        throw new Error("Fetch to Correlation Test failed");
    }
}

async function fetchCorrelationTest(fetch, correlationValues) {
    const chiSquaredSideBiasResponse = await fetch("/api/statistics/correlation/pearson", {
        method: "POST",
        body: JSON.stringify(correlationValues),
        headers: { "Content-Type": "application/json" },
    });
    if (chiSquaredSideBiasResponse.ok) {
        return chiSquaredSideBiasResponse.json();
    } else {
        console.error("Respuesta no exitosa: Correlation Test", chiSquaredSideBiasResponse.statusText);
        throw new Error("Fetch to Correlation Test failed");
    }
}

async function fetchDisplayModes(fetch, selectedSubject, uid, firestoreCollection, startDate, endDate) {
    const responseFetch = await fetch(`/api/statistics/display-modes?firestoreCollection=${firestoreCollection}&subject=${selectedSubject}&uid=${uid}&startDate=${startDate}&endDate=${endDate}`);
    if (responseFetch.ok) {
        return responseFetch.json();
    } else {
        console.error("Respuesta no exitosa: Display Modes", responseFetch.statusText);
        throw new Error("Fetch to Display Modes failed");
    }
}

// The GET function using the provided `fetch` for server-side fetching
export async function GET({ url, fetch }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const firestoreCollection = String(url.searchParams.get('firestoreCollection') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? undefined);
    const endDate = String(url.searchParams.get('endDate') ?? undefined);
    try {
        const valueCounts = await fetchValueCounts(fetch, selectedSubjectType, uid , firestoreCollection, startDate, endDate);
        const displayModes = await fetchDisplayModes(fetch, selectedSubjectType, uid , firestoreCollection, startDate, endDate);
        const binomialTest = await fetchBinominalTest(fetch, valueCounts);
        const contingencyTable = await fetchContingencyTable(fetch, selectedSubjectType, uid, firestoreCollection, startDate, endDate);
        const chiSquaredSideBias = await fetchChiSquaredSideBias(fetch, contingencyTable);
        const chiSquaredPearson = await fetchChiSquaredPearson(fetch, contingencyTable);
        const correlationValues = await fetchCorrelationValues(fetch, selectedSubjectType, uid, firestoreCollection, startDate, endDate);
        const correlationTest = await fetchCorrelationTest(fetch, correlationValues);

        // Packaging the response data
        const response = {
                valueCounts,
                displayModes,
                binomialTest,
                contingencyTable,
                chiSquaredSideBias,
                chiSquaredPearson,
                correlationValues,
                correlationTest,
        };
        // Returning the combined data as a JSON response
        return json(response);
    } catch (error) {
        console.error(error);
        // Returning an error response if any of the fetch operations fail
        return json({ error: 'An error occurred while fetching data.' }, { status: 500 });
    }
}
