import { json } from '@sveltejs/kit';
import jStat from 'jstat'; // Importing the default export

function calculateChiSquareTest(contingencyTable) {
    // Calculate totals and expected values
    const rowTotals = {};
    const colTotals = {};
    let grandTotal = 0;

    contingencyTable.forEach(row => {
        Object.keys(row).forEach(key => {
            if (key !== 'color') {
                const value = row[key];
                rowTotals[row.color] = (rowTotals[row.color] || 0) + value;
                colTotals[key] = (colTotals[key] || 0) + value;
                grandTotal += value;
            }
        });
    });

    const expectedFrequencies = {};
    let chiSquaredStatistic = 0;

    contingencyTable.forEach(row => {
        const color = row.color;
        expectedFrequencies[color] = {};

        Object.keys(row).forEach(key => {
            if (key !== 'color') {
                const expected = (rowTotals[color] * colTotals[key]) / grandTotal;
                expectedFrequencies[color][key] = expected;

                if (expected !== 0) {
                    const observed = row[key];
                    chiSquaredStatistic += (Math.pow(observed - expected, 2) / expected);
                }
            }
        });
    });

    const degreesOfFreedom = (Object.keys(rowTotals).length - 1) * (Object.keys(colTotals).length - 1);
    const pValue = 1 - jStat.chisquare.cdf(chiSquaredStatistic, degreesOfFreedom);

    return {
        chiSquaredStatistic,
        pValue,
        degreesOfFreedom,
        rowTotals,
        colTotals,
    };
}

export async function POST({ request }) {
    try {
        const body = await request.json();

        const result = calculateChiSquareTest(body.contingencyTable);

        return json({
            chiSquared: result.chiSquaredStatistic,
            pValue: result.pValue,
            degreesOfFreedom: result.degreesOfFreedom,
            lessThanSignificance: result.pValue < 0.05,
            colTotals: result.colTotals,
            rowTotals: result.rowTotals,
        });
    } catch (error) {
        console.error('Error processing request:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}
