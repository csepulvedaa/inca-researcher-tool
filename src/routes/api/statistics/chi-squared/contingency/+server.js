import { json } from "@sveltejs/kit";
import jStat from "jstat"; // Importing the default export

function calculateChiSquareTest(data) {
  // Initialize totals and frequencies
  const rowTotals = {};
  const colTotals = { left: 0, right: 0 };
  let grandTotal = 0;

  // Ensure each row (True/False) has 'left' and 'right' initialized
  Object.keys(data).forEach((key) => {
    data[key] = {
      left: data[key].left || 0,
      right: data[key].right || 0,
    };
  });

  // Calculate row totals and column totals
  Object.entries(data).forEach(([key, counts]) => {
    rowTotals[key] = 0;
    Object.entries(counts).forEach(([side, count]) => {
      rowTotals[key] += count;
      colTotals[side] = (colTotals[side] || 0) + count;
      grandTotal += count;
    });
  });

  // Calculate expected frequencies and chi-square statistic
  let chiSquaredStat = 0;
  Object.keys(data).forEach((row) => {
    Object.keys(data[row]).forEach((col) => {
      const expected = (rowTotals[row] * colTotals[col]) / grandTotal;
      const observed = data[row][col];
      chiSquaredStat += Math.pow(observed - expected, 2) / expected;
    });
  });

  // Degrees of freedom
  const degreesOfFreedom =
    (Object.keys(rowTotals).length - 1) * (Object.keys(colTotals).length - 1);

  // P-value from the chi-squared statistic
  const pValue = 1 - jStat.chisquare.cdf(chiSquaredStat, degreesOfFreedom);

  return {
    chiSquaredStat,
    degreesOfFreedom,
    pValue,
    rowTotals,
    colTotals,
  };
}

export async function POST({ request }) {
  try {
    const contingencyTable = await request.json();

    if (!contingencyTable) {
      return json({ error: "No contingency table provided" }, { status: 400 });
    }

    const result = calculateChiSquareTest(contingencyTable);

    const response = {
      chiSquared: result.chiSquaredStat,
      pValue: result.pValue,
      degreesOfFreedom: result.degreesOfFreedom,
      lessThanSignificance: result.pValue < 0.05,
      colTotals: result.colTotals,
      rowTotals: result.rowTotals,
    };

    return json(response);
  } catch (error) {
    console.error("Error processing request:", error);
    return json({ error: "An error occurred" }, { status: 500 });
  }
}
