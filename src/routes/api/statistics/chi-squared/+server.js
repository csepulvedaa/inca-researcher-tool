import { json } from "@sveltejs/kit";
import chiSquared from "chi-squared";

function calculateSideBiasChiSquare(contingencyTable) {
  // Step 1: Calculate totals for "right" and "left"
  const totals = {
    right: contingencyTable.right || 0,
    left: contingencyTable.left || 0,
  };
  const grandTotal = totals.right + totals.left;

  // Step 2: Calculate expected frequencies assuming no bias
  const expectedRight = grandTotal / 2;
  const expectedLeft = grandTotal / 2;

  // Step 3: Calculate the chi-square test statistic for right and left
  let chiSquaredTestStatistic =
    Math.pow(totals.right - expectedRight, 2) / expectedRight +
    Math.pow(totals.left - expectedLeft, 2) / expectedLeft;

  // Degrees of freedom for side bias is always 1 (two categories - 1)
  const degreesOfFreedom = 1;

  // Step 4: Calculate the p-value using the chi-square distribution
  const pValue = 1 - chiSquared.cdf(chiSquaredTestStatistic, degreesOfFreedom);

  // Return the result
  return {
    chiSquaredTestStatistic,
    degreesOfFreedom,
    pValue,
  };
}

export async function POST({ request }) {
  try {
    const body = await request.json();

    // Ensure that both True and False have left and right initialized to 0 if missing
    const totalRight = (body.True?.right || 0) + (body.False?.right || 0);
    const totalLeft = (body.True?.left || 0) + (body.False?.left || 0);

    const result = calculateSideBiasChiSquare({
      right: totalRight,
      left: totalLeft,
    });

    const significanceLevel = 0.05;
    const lessThanSignificance = result.pValue < significanceLevel;

    const response = {
      chiSquared: result.chiSquaredTestStatistic,
      pValue: result.pValue,
      degreesOfFreedom: result.degreesOfFreedom,
      lessThanSignificance: lessThanSignificance,
      totals: {
        right: totalRight,
        left: totalLeft,
      },
    };

    return json(response);
  } catch (error) {
    console.error("Error processing request:", error);
    return json({ error: "An error occurred" }, { status: 500 });
  }
}
