import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService'; // Adjust the import path as necessary

function getOptions(action) {
    return action.details.optionsC || action.details.options;
}

function calculateCorrelationData(actions) {
    const comb_list = getUniqueTwoItemCombinations(actions);
    const data_list = [];

    for (const [val1, val2] of comb_list) {
        const usable_actions = actions.filter(action => action.action === 'Option Clicked');

        const temp_df = usable_actions.filter((action) => {
            const options = getOptions(action);
            const isOptionsC = options.hasOwnProperty('c0') && options.hasOwnProperty('c1');
            const isOptionsV = options.hasOwnProperty('v0') && options.hasOwnProperty('v1');

            if (isOptionsC) {
                return ((options.c0 === val1 && options.c1 === val2 && options.c2 === 'none') ||
                    (options.c0 === val2 && options.c1 === val1 && options.c2 === 'none'));
            } else if (isOptionsV) {
                return ((options.v0 === val1 && options.v1 === val2 && options.v2 === 'none') ||
                    (options.v0 === val2 && options.v1 === val1 && options.v2 === 'none'));
            }
            return false;
        });

        if (temp_df.length > 0) {
            const true_values = temp_df.reduce((sum, action) => sum + (action.details.result ? 1 : 0), 0);
            const total = temp_df.length;
            const percentage = ((true_values / total) * 100).toFixed(2);
            const dot_sum = val1 + val2;
            const dot_dif = Math.abs(val1 - val2);
            const ratio = (Math.min(val1, val2) / Math.max(val1, val2)).toFixed(2);

            data_list.push({ 'Value 1': val1, 'Value 2': val2, 'Total': dot_sum, 'Difference': dot_dif, 'Ratio': ratio, 'Accuracy': percentage, 'Total Experiments': total });
        }
    }

    return data_list;
}

function getUniqueTwoItemCombinations(actions) {
    const uniqueValues = new Set();
    actions.forEach(action => {
        if (action.action !== "Option Clicked") {
            return;
        }
        const options = getOptions(action);
        if (options.hasOwnProperty('c0') && options.hasOwnProperty('c1')) {
            uniqueValues.add(options.c0);
            uniqueValues.add(options.c1);
        } else if (options.hasOwnProperty('v0') && options.hasOwnProperty('v1')) {
            uniqueValues.add(options.v0);
            uniqueValues.add(options.v1);
        }
    });

    const uniqueValuesArray = Array.from(uniqueValues);
    const combinations = [];
    for (let i = 0; i < uniqueValuesArray.length; i++) {
        for (let j = i + 1; j < uniqueValuesArray.length; j++) {
            combinations.push([uniqueValuesArray[i], uniqueValuesArray[j]]);
        }
    }

    return combinations;
}

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const firestoreCollection = String(url.searchParams.get('firestoreCollection') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');

    try {
        let actions;
        if ( startDate !== 'undefined' && endDate !== 'undefined' ) {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid, startDate, endDate);
        }
        else {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid);
        }        
        const correlationData = calculateCorrelationData(actions);

        return json(correlationData);
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}