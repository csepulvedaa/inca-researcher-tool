import { json } from '@sveltejs/kit';

function pearsonCorrelation(data, keys) {
    // Function to calculate mean
    const mean = (values) => values.reduce((sum, value) => sum + value, 0) / values.length;
  
    // Function to calculate standard deviation
    const stdDev = (values, mean) => Math.sqrt(values.map(x => Math.pow(x - mean, 2)).reduce((sum, value) => sum + value, 0) / values.length);
  
    // Function to calculate covariance
    const covariance = (values1, values2, mean1, mean2) => values1.map((x, i) => (x - mean1) * (values2[i] - mean2)).reduce((sum, value) => sum + value, 0) / values1.length;
  
    // Initialize the correlation matrix
    let correlationMatrix = {};
  
    // Iterate over each combination of keys for the matrix
    keys.forEach((keyX) => {
      correlationMatrix[keyX] = {};
  
      keys.forEach((keyY) => {
        // Extract values for both keys
        let valuesX = data.map(item => parseFloat(item[keyX])).filter(val => !isNaN(val));
        let valuesY = data.map(item => parseFloat(item[keyY])).filter(val => !isNaN(val));
  
        // Calculate means
        let meanX = mean(valuesX);
        let meanY = mean(valuesY);
  
        // Calculate standard deviations
        let stdDevX = stdDev(valuesX, meanX);
        let stdDevY = stdDev(valuesY, meanY);
  
        // Calculate covariance
        let covXY = covariance(valuesX, valuesY, meanX, meanY);
  
        // Calculate correlation coefficient
        correlationMatrix[keyX][keyY] = parseFloat((covXY / (stdDevX * stdDevY)).toFixed(14));
    });
    });
  
    return correlationMatrix;
  }


  
export async function POST({ request }) {
  try {
    const body = await request.json();

    let keys = ['Total', 'Difference', 'Ratio', 'Accuracy'];

    let correlationMatrix = pearsonCorrelation(body, keys);

    return json(correlationMatrix);
  } catch (error) {
    console.error(error);
    return null;
  }
}
