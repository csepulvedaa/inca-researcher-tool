import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';
import binomialTest from '@stdlib/stats-binomial-test';

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const firestoreCollection = String(url.searchParams.get('firestoreCollection') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');

    try {
        let actions;
        if (startDate !== 'undefined' && endDate !== 'undefined') {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid, startDate, endDate);
        } else {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid);
        }

        // Filter actions by specific game-ending types
        const gameEndedActions = actions.filter(action => action.action === 'Exit game' || action.action === 'End game');

        // Calculate accuracy and totalize by gameId
        const gameAccuracy = gameEndedActions.map(game => {
            const details = game.details;
            const date = game.date;
            const time = game.time;
            const normalPValue = binomialTest(details.normalPoppedTotal, details.normalTotal).pValue;
            const experimentalPValue = binomialTest(details.experimentalPoppedTotal, details.experimentalTotal).pValue;
            const controlPValue = binomialTest(details.controlPoppedTotal, details.controlTotal).pValue;
            const markedPValue = binomialTest(details.experimentalPoppedTotal + details.controlPoppedTotal, details.experimentalTotal + details.controlTotal).pValue;
            return {
                gameId: details.gameId,
                date,
                time,
                normal_accuracy: details.normalPoppedTotal / details.normalTotal,
                experimental_accuracy: details.experimentalPoppedTotal / details.experimentalTotal,
                control_accuracy: details.controlPoppedTotal / details.controlTotal,
                marked_accuracy: (details.experimentalPoppedTotal + details.controlPoppedTotal) / (details.experimentalTotal + details.controlTotal),
                pValues: {
                    normalPValue,
                    experimentalPValue,
                    controlPValue,
                    markedPValue,
                }
            };
        });

        // Calculate global totals
        const totalNormalPopped = gameEndedActions.reduce((acc, game) => acc + game.details.normalPoppedTotal, 0);
        const totalNormal = gameEndedActions.reduce((acc, game) => acc + game.details.normalTotal, 0);
        const totalExperimentalPopped = gameEndedActions.reduce((acc, game) => acc + game.details.experimentalPoppedTotal, 0);
        const totalExperimental = gameEndedActions.reduce((acc, game) => acc + game.details.experimentalTotal, 0);
        const totalControlPopped = gameEndedActions.reduce((acc, game) => acc + game.details.controlPoppedTotal, 0);
        const totalControl = gameEndedActions.reduce((acc, game) => acc + game.details.controlTotal, 0);

        const totalNormalAccuracy = totalNormalPopped / totalNormal;
        const totalExperimentalAccuracy = totalExperimentalPopped / totalExperimental;
        const totalControlAccuracy = totalControlPopped / totalControl;


        // Perform binomial tests for totals
        const normalPValue = binomialTest(totalNormalPopped, totalNormal).pValue;
        const experimentalPValue = binomialTest(totalExperimentalPopped, totalExperimental).pValue;
        const controlPValue = binomialTest(totalControlPopped, totalControl).pValue;

        // Calculate combined accuracy and p-value for marked elements (experimental + control)
        const totalMarkedPopped = totalExperimentalPopped + totalControlPopped;
        const totalMarked = totalExperimental + totalControl;
        const totalMarkedAccuracy = totalMarkedPopped / totalMarked;
        const markedPValue = binomialTest(totalMarkedPopped, totalMarked).pValue;

        // Return individual results by gameId and totals with p-values
        return json({
            gameAccuracy,
            totals: {
                totalNormalAccuracy,
                totalExperimentalAccuracy,
                totalControlAccuracy,
                totalMarkedAccuracy,
                pValues: {
                    normalPValue,
                    experimentalPValue,
                    controlPValue,
                    markedPValue
                }
            }
        });

    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}
