import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const firestoreCollection = String(url.searchParams.get('firestoreCollection') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');
    try {
        let actions;
        if ( startDate !== 'undefined' && endDate !== 'undefined' ) {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid, startDate, endDate);
        }
        else {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid);
        }
        // Initialize an object to hold the results grouped by numberOfChoices
        const groupedResults = actions.reduce((acc, item) => {
            const details = item.details || {};
            const { numberOfChoices } = details;
            const selection = details.result;

            // Skip processing if numberOfChoices is undefined or null
            if (numberOfChoices == null) {
                return acc;
            }

            // Initialize the group if it doesn't exist
            if (!acc[numberOfChoices]) {
                acc[numberOfChoices] = { correct: 0, incorrect: 0 };
            }

            // Increment the correct or incorrect count based on the selection
            if (selection) acc[numberOfChoices].correct++;
            else acc[numberOfChoices].incorrect++;

            return acc;
        }, {});

        // Calculate percentages for each group
        const resultWithPercentage = Object.keys(groupedResults).reduce((acc, key) => {
            const totalItems = groupedResults[key].correct + groupedResults[key].incorrect;
            acc[key] = {
                data: groupedResults[key],
                percentage: {
                    correct: parseFloat(((groupedResults[key].correct / totalItems) * 100).toFixed(2)),
                    incorrect: parseFloat(((groupedResults[key].incorrect / totalItems) * 100).toFixed(2)),
                }
            };
            return acc;
        }, {});

        return json(resultWithPercentage);
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}
