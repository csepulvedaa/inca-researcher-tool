import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';
import { INCA_APPS } from '$lib/constants.js';

const poppedElementCollection = INCA_APPS.POP;

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');

    try {
        let poppedElementActions;

        if (startDate !== 'undefined' && endDate !== 'undefined') {
            poppedElementActions = await fetchAndCacheFirestoreData(poppedElementCollection, selectedSubjectType, uid, startDate, endDate);
        } else {
            poppedElementActions = await fetchAndCacheFirestoreData(poppedElementCollection, selectedSubjectType, uid);
        }

        // Filter poppedElementActions to only include "Popped element" actions and remove where innerFigColor is undefined
        poppedElementActions = poppedElementActions.filter(action => action.action === 'Popped element' && action.details.innerFigColor !== undefined);

        // Summarize poppedElementActions
        const poppedSummary = poppedElementActions.reduce((acc, action) => {
            const { color, innerFigColor, onScreenExactlySameAsPopped, onScreenDiffButSameTypeAsPopped, onScreenDifferentTypeAsPopped, onScreenElmnts } = action.details;
            const key = `${color},${innerFigColor}`;
            if (!acc[key]) {
                acc[key] = { color, innerFigColor, prob_identical: 0, prob_same_type: 0, prob_different: 0, prob_total_elements: 0, count: 0 };
            }
            acc[key].prob_identical += 1 / (onScreenExactlySameAsPopped + 1);
            acc[key].prob_same_type += 1 / (onScreenDiffButSameTypeAsPopped);
            acc[key].prob_different += 1 / (onScreenDifferentTypeAsPopped + 1);
            acc[key].prob_total_elements += 1 / (onScreenElmnts);
            acc[key].count += 1;
            return acc;
        }, {});

        // Calculate average probabilities
        const combinedSummary = Object.values(poppedSummary).map(({ color, innerFigColor, prob_identical, prob_same_type, prob_different, prob_total_elements, count }) => ({
            color,
            innerFigColor,
            prob_identical: prob_identical / count,
            prob_same_type: prob_same_type / count,
            prob_different: prob_different / count,
            prob_total_elements: prob_total_elements / count,
            count,
        }));

        // Sort the combined summary
        combinedSummary.sort((a, b) => a.color.localeCompare(b.color));

        return json(combinedSummary);
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}