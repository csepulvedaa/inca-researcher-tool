import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';
import { INCA_APPS } from '$lib/constants.js';

const poppedElementCollection = INCA_APPS.POP;
const endGameCollection = INCA_APPS.POP_END;

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');
    try {
        let poppedElementActions;
        let endGameActions;

        if (startDate !== 'undefined' && endDate !== 'undefined') {
            poppedElementActions = await fetchAndCacheFirestoreData(poppedElementCollection, selectedSubjectType, uid, startDate, endDate);
            endGameActions = await fetchAndCacheFirestoreData(endGameCollection, selectedSubjectType, uid, startDate, endDate);
        }
        else {
            poppedElementActions = await fetchAndCacheFirestoreData(poppedElementCollection, selectedSubjectType, uid);
            endGameActions = await fetchAndCacheFirestoreData(endGameCollection, selectedSubjectType, uid);
        }

        // Filter poppedElementActions to only include "Popped element" actions and remove where innerFigColor is undefined
        poppedElementActions = poppedElementActions.filter(action => action.action === 'Popped element' && action.details.innerFigColor !== undefined);

        // Summarize poppedElementActions
        const poppedSummary = poppedElementActions.reduce((acc, action) => {
            const { color, innerFigColor } = action.details;
            const key = `${color},${innerFigColor}`;
            if (!acc[key]) {
                acc[key] = { color, innerFigColor, popped: 0 };
            }
            acc[key].popped += 1;
            return acc;
        }, {});

        // Summarize endGameActions
        const seenSummary = endGameActions.reduce((acc, action) => {
            const specialColorPairsSeen = action.details.specialColorPairsSeen;
            for (const [key, seen] of Object.entries(specialColorPairsSeen)) {
                const [color, innerFigColor] = key.split(',');
                if (!acc[key]) {
                    acc[key] = { color, innerFigColor, seen: 0 };
                }
                acc[key].seen += seen;
            }
            return acc;
        }, {});

        // Combine summaries
        const combinedSummary = Object.keys({ ...poppedSummary, ...seenSummary }).map(key => {
            const popped = poppedSummary[key] ? poppedSummary[key].popped : 0;
            const seen = seenSummary[key] ? seenSummary[key].seen : 0;
            const popped_per_seen = seen ? popped / seen : 0;
            return {
                color: key.split(',')[0],
                innerFigColor: key.split(',')[1],
                popped,
                seen,
                popped_per_seen
            };
        }).filter(row => row.seen > 0);  // Filter out entries where seen is 0

        // Sort the combined summary
        combinedSummary.sort((a, b) => a.color.localeCompare(b.color));

        return json(combinedSummary);
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}
