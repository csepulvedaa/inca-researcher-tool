import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';
import { INCA_APPS } from '$lib/constants.js';

const poppedElementCollection = INCA_APPS.POP;

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');
    try {
        let poppedElementActions;

        if (startDate !== 'undefined' && endDate !== 'undefined') {
            poppedElementActions = await fetchAndCacheFirestoreData(poppedElementCollection, selectedSubjectType, uid, startDate, endDate);
        }
        else {
            poppedElementActions = await fetchAndCacheFirestoreData(poppedElementCollection, selectedSubjectType, uid);
        }

        // Filter and map to get the coordinates
        const xCoordinates = [];
        const yCoordinates = [];
        poppedElementActions
            .filter(action => action.action === 'Popped element')
            .forEach(action => {
                xCoordinates.push(action.details.x);
                yCoordinates.push(action.details.y);
            });

        const coordinates = { x: xCoordinates, y: yCoordinates };

        return json(coordinates);
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}
