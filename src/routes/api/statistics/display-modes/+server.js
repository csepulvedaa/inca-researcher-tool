import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';


export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const firestoreCollection = String(url.searchParams.get('firestoreCollection') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');

    try {
        let actions;
        if ( startDate !== 'undefined' && endDate !== 'undefined' ) {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid, startDate, endDate);
        }
        else {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid);
        }
        const groupedResults = actions.reduce((acc, item) => {
            const { numberOfChoices, testName } = item.details;

            if (numberOfChoices == null) {
                return acc;
            }
            
            if (!acc[numberOfChoices]) {
                acc[numberOfChoices] = {};
            }

            if (!acc[numberOfChoices][testName]) {
                acc[numberOfChoices][testName] = 1;
            } else {
                acc[numberOfChoices][testName]++;
            }

            return acc;
        }, {});

        return json(groupedResults);
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}
