// src/routes/api/statistics/contingency-table/+server.js
import { json } from "@sveltejs/kit";
import { fetchAndCacheFirestoreData } from "$lib/firestore/sharedFirestoreService";

function calculateContingencyTable(data) {
  const contingencyTable = {};

  data.forEach((entry) => {
    if (entry.action !== "Option Clicked") {
      return;
    }
    const result = entry.details.result ? "True" : "False";
    const side = entry.details.side || "unknown";
    if (side === "unknown") {
      return;
    }
    contingencyTable[result] = contingencyTable[result] || {};
    contingencyTable[result][side] = (contingencyTable[result][side] || 0) + 1;
  });

  return contingencyTable;
}

export async function GET({ url }) {
  const selectedSubjectType = String(url.searchParams.get("subject") ?? "");
  const firestoreCollection = String(
    url.searchParams.get("firestoreCollection") ?? ""
  );
  const startDate = String(url.searchParams.get("startDate") ?? "");
  const endDate = String(url.searchParams.get("endDate") ?? "");

  const uid = String(url.searchParams.get("uid") ?? "");

  try {
    let actions;
    if (startDate !== "undefined" && endDate !== "undefined") {
      actions = await fetchAndCacheFirestoreData(
        firestoreCollection,
        selectedSubjectType,
        uid,
        startDate,
        endDate
      );
    } else {
      actions = await fetchAndCacheFirestoreData(
        firestoreCollection,
        selectedSubjectType,
        uid
      );
    }
    const contingencyTable = calculateContingencyTable(actions);
    return json(contingencyTable);
  } catch (error) {
    console.error("Error fetching data:", error);
    return json({ error: "An error occurred" }, { status: 500 });
  }
}
