import { json } from '@sveltejs/kit';
import { fetchAndCacheFirestoreData } from '$lib/firestore/sharedFirestoreService';

export async function GET({ url }) {
    const selectedSubjectType = String(url.searchParams.get('subject') ?? '');
    const firestoreCollection = String(url.searchParams.get('firestoreCollection') ?? '');
    const uid = String(url.searchParams.get('uid') ?? '');
    const startDate = String(url.searchParams.get('startDate') ?? '');
    const endDate = String(url.searchParams.get('endDate') ?? '');

    try {
        let actions;
        if (startDate !== 'undefined' && endDate !== 'undefined') {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid, startDate, endDate);
        } else {
            actions = await fetchAndCacheFirestoreData(firestoreCollection, selectedSubjectType, uid);
        }

        // Filtrar acciones por 'Popped Element'
        const poppedElementActions = actions.filter(action => action.action === 'Popped element');

        // Identificar todos los colores internos posibles
        const allInnerColors = new Set();
        poppedElementActions.forEach(action => {
            allInnerColors.add(action.details.innerFigColor);
        });

        // Crear tabla de contingencia con todos los colores internos inicializados a cero
        const contingencyTable = {};
        poppedElementActions.forEach(action => {
            const color = action.details.color;
            if (!contingencyTable[color]) {
                contingencyTable[color] = {};
                allInnerColors.forEach(innerColor => {
                    contingencyTable[color][innerColor] = 0;
                });
            }
            contingencyTable[color][action.details.innerFigColor]++;
        });

        // Formatear la tabla para el frontend
        const formattedTable = Object.keys(contingencyTable).map(color => {
            return { color, ...contingencyTable[color] };
        });

        return json({ contingencyTable: formattedTable });
    } catch (error) {
        console.error('Error fetching data:', error);
        return json({ error: 'An error occurred' }, { status: 500 });
    }
}

