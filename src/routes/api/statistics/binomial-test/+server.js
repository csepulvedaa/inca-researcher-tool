import { json } from '@sveltejs/kit';
import binomialTest from '@stdlib/stats-binomial-test';

export async function POST({ request }) {
    try {
        const body = await request.json();
        
        // Initialize an object to hold the results for each numberOfChoices group
        const response = {};

        // The significance level for the binomial test
        const significanceLevel = 0.05;

        // Iterate over each group in the body
        for (const numberOfChoices in body) {
            const { data } = body[numberOfChoices];
            const { correct, incorrect } = data;

            const k_all = correct;
            const n_all = incorrect + correct;

            const res = binomialTest(k_all, n_all);
            const notChoosedRandom = res.pValue < significanceLevel;

            // Store the result for the current group in the response object
            response[numberOfChoices] = {
                pValue: res.pValue,
                notChoosedRandom,
            };
        }

        return json(response);

    } catch (error) {
        console.error('Error:', error);
        return json({ error: error.message }, { status: 500 });
    }
}
