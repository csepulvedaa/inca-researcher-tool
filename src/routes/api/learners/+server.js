// Import the cache utilities
import { error, json } from '@sveltejs/kit';
import { db } from '$lib/firebase/firebase'; // Adjust this import to your Firestore client SDK setup
import { doc, getDoc } from 'firebase/firestore';
import { getCache, setCache } from '$lib/cache/cache'; // Adjust the import path as necessary


const CACHE_MAX_AGE = import.meta.env.VITE_CACHE_MAX_AGE; // 1 minute


export async function GET({ url }) {
    const uid = url.searchParams.get('uid');
    if (!uid) {
        throw error(400, 'Missing UID');
    }

    // Define a unique cache key for each user's learners data
    const cacheKey = `learners-${uid}`;
    // Define the maximum age of cache data in milliseconds (e.g., 1000 * 60 = 1 minute)

    // Try to retrieve data from cache first
    const cachedData = getCache(cacheKey, CACHE_MAX_AGE);
    if (cachedData) {
        console.log('Returning data from cache', cacheKey);
        return json({learners: cachedData});
    }

    try {
        const userDocRef = doc(db, "users", uid);
        const docSnap = await getDoc(userDocRef);

        if (!docSnap.exists()) {
            throw error(404, 'User not found');
        }

        const userData = docSnap.data();
        // Update the cache with the new data
        setCache(cacheKey, userData.learners || []);
        return json({ learners: userData.learners || [] });
    } catch (err) {
        console.error('Error fetching learners:', err);
        throw error(500, 'Internal Server Error');
    }
}
