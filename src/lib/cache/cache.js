// cache.js
const cache = new Map();

function setCache(key, data) {
  // console.log('set cache func', key, data)
  cache.set(key, { data, timestamp: Date.now() });
}

function getCache(key, maxAge) {
  const cachedData = cache.get(key);
  if (cachedData) {
    const isExpired = Date.now() - cachedData.timestamp >= maxAge;
    if (!isExpired) {
      // console.log('Cache hit for key:', key);
      return cachedData.data;
    } else {
      // Expired, clear it right away
      // console.log('Cache expired for key:', key);
      cache.delete(key);
    }
  }
  // console.log('Cache miss for key:', key);
  return null;
}


export { setCache, getCache };
