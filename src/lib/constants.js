export let INCA_APPS = {
    DELBOEUF:`Delboeuf-${import.meta.env.VITE_DELBOEUF_VERSION}`,
    WHICH_HAS_MORE: `Which-has-more-${import.meta.env.VITE_WHICH_HAS_MORE_VERSION}`,
    POP : `inca-pop-${import.meta.env.VITE_POP_VERSION}`,
    POP_END : `inca-pop-exit-end-${import.meta.env.VITE_POP_VERSION}`,
    CIO: `Click-in-Order${import.meta.env.VITE_CIO_VERSION}`
}