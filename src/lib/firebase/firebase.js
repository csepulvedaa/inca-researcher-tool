// @ts-nocheck
import { deleteApp, getApp, getApps, initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";
import { getFirestore, serverTimestamp, addDoc, collection } from "firebase/firestore";

const firebaseConfig = {
  apiKey: import.meta.env.VITE_APIKEY,
  authDomain: import.meta.env.VITE_AUTH_DOMAIN,
  projectId: import.meta.env.VITE_PROJECT_ID,
  storageBucket: import.meta.env.VITE_STORAGE_BUCKET,
  messagingSenderId: import.meta.env.VITE_MESSAGE_SENDER_ID,
  appId: import.meta.env.VITE_APP_ID,
};

let firebaseApp;
if (!getApps().length) {
  firebaseApp = initializeApp(firebaseConfig);
} else {
  firebaseApp = getApp();
  deleteApp(firebaseApp);
  firebaseApp = initializeApp(firebaseConfig);
}

export async function SendToFirebase(user, teacher, learner, action, results) {
  let eventFb;
  try {
    const now = new Date();
    let event = {
      date: now.toLocaleDateString(),
      time: now.toLocaleTimeString(),
      teacher: teacher,
      subject: learner,
      action: `${action}`,
      details: { ...results },
    }
    // saveLocally(event);
    eventFb = {
      timestamp: serverTimestamp(),
      user: user ? user.displayName : "Anonymous",
      userId: user ? user.uid : "Anonymous",
      ...event,
    }
    await addDoc(collection(db, `Which-has-more-2.1`), eventFb);
    console.log("Event sent to firebase!");
  } catch (error) {
    console.log("Error while sending event:", eventFb, "to firebase: ", error);
  }
}


export const db = getFirestore(firebaseApp);
export const auth = getAuth(firebaseApp);