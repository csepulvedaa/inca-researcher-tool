<script>
    import DownloadButton from "$lib/components/ui/DownloadButton.svelte";
    export let actions;

    // Preparamos los headers y los datos de la tabla
    const tableHeaders = [
        "Value 1",
        "Value 2",
        "Total",
        "Difference",
        "Ratio",
        "Accuracy",
        "Total Experiments",
    ];

    const tableData = actions.correlationValues.map((test) => [
        test["Value 1"],
        test["Value 2"],
        test.Total,
        test.Difference,
        test.Ratio,
        test.Accuracy + "%",
        test["Total Experiments"],
    ]);
</script>

{#if actions.correlationValues}
    <section>
        <div class="table-header">
            <div class="header-text">
                <h2>Correlation Test</h2>
            </div>

            <div class="download-button">
                <DownloadButton
                    name="Correlation Test"
                    tableId="correlation-table"
                    {tableHeaders}
                    {tableData}
                />
            </div>
        </div>

        <table class="table table-dark" id="correlation-table">
            <thead>
                <tr>
                    {#each tableHeaders as header}
                        <th>{header}</th>
                    {/each}
                </tr>
            </thead>
            <tbody>
                {#each actions.correlationValues as test}
                    <tr>
                        <td>{test["Value 1"]}</td>
                        <td>{test["Value 2"]}</td>
                        <td>{test.Total}</td>
                        <td>{test.Difference}</td>
                        <td>{test.Ratio}</td>
                        <td>{test.Accuracy}%</td>
                        <td>{test["Total Experiments"]}</td>
                    </tr>
                {/each}
            </tbody>
        </table>
    </section>
{/if}

<style>
    .download-button {
        display: flex;
        justify-content: flex-end;
        margin-bottom: 1rem;
    }
    .table-header {
        display: flex;
        justify-content: space-between;
        margin-bottom: auto;
    }

    table {
        width: 100%;
    }

    th {
        text-align: center;
    }

    td {
        text-align: center;
    }
</style>
