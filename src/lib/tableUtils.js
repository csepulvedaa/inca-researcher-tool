export function prepareData(items, columns) {
    const jsonToString = (obj) => {
        return Object.keys(obj)
            .sort()
            .map(key => `${key}: ${typeof obj[key] === 'object' ? JSON.stringify(obj[key]) : obj[key]}`)
            .join(', ');
    };

    const safeToString = (value) => {
        if (value === null || value === undefined) {
            return "";
        } else if (typeof value === 'boolean' || typeof value === 'number') {
            return value.toString();
        } else if (typeof value === 'object') {
            // Convierte el objeto a una cadena formateada
            return jsonToString(value);
        } else {
            return value.toString();
        }
    };

    const getNestedValue = (item, path) => {
        return path.split('.').reduce((acc, part) => acc && acc.hasOwnProperty(part) ? acc[part] : null, item);
    };

    return items.map((item) => {
        return columns.reduce((acc, column) => {
            let value;
            if (item.details && item.details.hasOwnProperty(column.accessor)) {
                value = getNestedValue(item.details, column.accessor);
            } else {
                value = getNestedValue(item, column.accessor);
            }
            acc[column.accessor] = safeToString(value);
            return acc;
        }, {});
    });
}


export function formatColumns(columnHeaders) {
    // Convert header string to camelCase for the accessor
    function toCamelCase(str) {
        return (
            str
                // Lower cases the string
                .toLowerCase()
                // Splits the string into words
                .split(" ")
                // Removes the leading and trailing whitespace
                .map((word, index) => {
                    // If it is the first word make sure it is in lowercase
                    if (index === 0) {
                        return word.toLowerCase();
                    }
                    // Capitalizes the first letter of each word and adds the rest of the word
                    return word.charAt(0).toUpperCase() + word.slice(1);
                })
                // Joins all the words to make a single string
                .join("")
        );
    }

    // Map each header to an object with header and accessor keys
    return columnHeaders.map((header) => ({
        header: header, // The human-readable string
        accessor: toCamelCase(header), // Convert header to camelCase accessor
    }));
}

export function convertTableToLaTeX(headers, data) {
    function escapeLatex(str) {
        return str.replace(/%/g, "\\%").replace(/#/g, "\\#");
    }

    let latex = "\\begin{table}[h]\n\\centering\n";
    latex +=
        "\\begin{tabular}{|" +
        headers.map(() => "c").join("|") +
        "|}\n\\hline\n";
    latex +=
        headers.map((header) => escapeLatex(header)).join(" & ") +
        " \\\\ \\hline\n";
    data.forEach((row) => {
        latex +=
            row.map((cell) => escapeLatex(cell.toString())).join(" & ") +
            " \\\\ \\hline\n";
    });
    latex += "\\end{tabular}\n";
    latex += "\\caption{Caption for your table}\n";
    latex += "\\label{table:your_label}\n";
    latex += "\\end{table}";
    return latex;
}

