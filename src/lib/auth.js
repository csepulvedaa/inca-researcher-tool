// src/lib/auth.js
import { auth } from '$lib/firebase/firebase';

export const onAuthStateChanged = (callback) => {
  return auth.onAuthStateChanged(callback);
};

export async function fetchLearners(uid) {
  if (!uid) {
    console.error("User ID is required to fetch learners.");
    return [];
  }

  const response = await fetch(`/api/learners?uid=${uid}`);
  if (response.ok) {
    const { learners } = await response.json();
    return learners;
  } else {
    console.error("Failed to fetch learners");
    return [];
  }
}
