import { Parser } from '@json2csv/plainjs';
import JSZip from 'jszip';

export const logsToJSONUrl = (logObject) => {
    return URL.createObjectURL(
      new Blob([JSON.stringify(logObject, null, 2)], {
        type: 'application/json',
      })
    );
  };


  
  export const asyncLogsToCsvUrl = async (logsArray) => {
      // Create a new JSZip instance
      const zip = new JSZip();
      
      // Process logs to include all fields except for 'timestamp' and 'userId', 
      // and flatten the 'details' object
      const processedLogs = logsArray.map(log => {
          const { timestamp, userId, details, ...otherFields } = log;
          return { ...otherFields, ...details };
      });
  
      // Initialize the parser with options
      const parser = new Parser();
  
      // Convert the processed logs to CSV
      const csv = parser.parse(processedLogs);
  
      // Assuming all logs have the same subject for filename
      const filename = 'LogDetails.csv';
      zip.file(filename, csv);
  
      // Generate the zip file and return its URL
      const content = await zip.generateAsync({ type: 'blob' });
      return URL.createObjectURL(content);
  };
  
  // This code snippet will not run directly in this notebook as it requires certain 
  // Node.js packages ('json2csv' and 'jszip') and a DOM environment for `URL.createObjectURL`.
  
  
  