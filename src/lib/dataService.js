// src/lib/dataService.js

export async function fetchBFFData( subject, userId, firestoreCollection, startDate, endDate) {
  const response = await fetch(`/api/bff?subject=${subject}&uid=${userId}&firestoreCollection=${firestoreCollection}&startDate=${startDate}&endDate=${endDate}`);
  if (response.ok) {
    return await response.json();
  } else {
    console.error("Failed to fetch data", response.statusText);
    return null;
  }
}

export async function fetchPopData( subject, userId, startDate, endDate) {
  const response = await fetch(`/api/bff/pop?subject=${subject}&uid=${userId}&startDate=${startDate}&endDate=${endDate}`);
  if (response.ok) {
    return await response.json();
  } else {
    console.error("Failed to fetch data", response.statusText);
    return null;
  }
}

