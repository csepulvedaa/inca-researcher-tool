import { db } from "$lib/firebase/firebase";
import { collection, query, where, getDocs } from "firebase/firestore";
import { setCache, getCache } from "$lib/cache/cache";

const CACHE_MAX_AGE = import.meta.env.VITE_CACHE_MAX_AGE; // Example: 1 minute

export async function fetchAndCacheFirestoreData(
  firestoreCollection,
  subjectType,
  userId,
  startDate,
  endDate
) {
  const cacheKey = `firestoreData_${firestoreCollection}_${subjectType}_${userId}`;
  let cachedData = getCache(cacheKey, CACHE_MAX_AGE);
  if (cachedData) {
    console.log("Returning data from cache", cacheKey);
  } else {
    // Inicia la construcción de la query base con el userId
    let conditions = [where("userId", "==", userId)];

    // Añade el subjectType a la consulta si está presente
    if (subjectType !== "All") {
      conditions.push(where("subject", "==", subjectType));
    }

    // Realiza la consulta básica a Firestore
    const q = query(collection(db, firestoreCollection), ...conditions);
    const querySnapshot = await getDocs(q);
    cachedData = querySnapshot.docs.map((doc) => doc.data());
    setCache(cacheKey, cachedData); // Cache the fetched data
  }

  // Filtro adicional si la colección contiene "Delboeuf"
  if (firestoreCollection.includes("Delboeuf")) {
    cachedData = cachedData
      .map((item) => {
        if (item.action === "Option Clicked") {
          if (
            item.details &&
            item.details.optionsV &&
            item.details.optionsV.c0 === item.details.optionsV.c1
          ) {
            return item;
          } else {
            return null;
          }
        }
        return item;
      })
      .filter((item) => item !== null);
  }

  // If startDate and endDate are provided, filter the cached/fetched data
  if (
    startDate &&
    endDate &&
    startDate !== undefined &&
    endDate !== undefined
  ) {
    const startTimestamp = new Date(startDate).getTime();
    // Add one day's worth of milliseconds to include the end date
    const endTimestamp = new Date(endDate).getTime() + 86400000;

    // Filter the data based on the timestamp range
    cachedData = cachedData.filter((item) => {
      const itemTimestamp =
        item.timestamp.seconds * 1000 + item.timestamp.nanoseconds / 1000000;
      return itemTimestamp >= startTimestamp && itemTimestamp < endTimestamp; // Use less than for endTimestamp
    });
  }

  // Format items.date and items.time
  cachedData = cachedData.map((item) => {
    const timestamp = new Date(
      item.timestamp.seconds * 1000 + item.timestamp.nanoseconds / 1000000
    );

    // Format date as YYYY/MM/DD if not already
    if (!/^\d{4}\/\d{2}\/\d{2}$/.test(item.date)) {
      item.date = timestamp.toISOString().split("T")[0].replace(/-/g, "/");
    }

    // Format time as HH:MM:SS if not already
    if (!/^\d{2}:\d{2}:\d{2}$/.test(item.time)) {
      item.time = timestamp.toTimeString().split(" ")[0];
    }

    return item;
  });

  return cachedData; // Return the filtered or original data
}
