import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  title: "InCA - Researcher Tool",
  description: "dvanced web application designed to facilitate the collection, management, and analysis of behavioral and cognitive data from digital life enrichment applications.",
  outDir: '../public',
  base: '/inca-researcher-tool/',
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      { text: 'Home', link: '/' },
    ],

    sidebar: [
      {
        text: 'Examples',
        items: [
          { text: 'Add An Application', link: '/add-an-app' },
          { text: 'Default Page', link: '/defaultpage' },
          { text: 'Runtime API Examples', link: '/api-examples' },
          { text: 'Enviroment Variables', link: '/enviroment' }
        ]
      }
    ],

    socialLinks: [
      { icon: 'github', link: 'https://github.com/vuejs/vitepress' }
    ]
  }
})
