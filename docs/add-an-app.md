# How to Add a New Page Using `DefaultPage` or Other Page Components

This guide explains how to add a new page to the project using the `DefaultPage` component or any other custom page component connected through exported constants. The project follows the [SvelteKit routing](https://kit.svelte.dev/docs/routing) structure, where each route corresponds to a directory inside `src/routes/`.

## 1. Create a New Route Folder

To add a new page, you must first create a folder for the route inside `src/routes`. For example, to add a new page for an application:

```bash
src/
  routes/
    new-app/       <-- Create a new folder for the route
      +page.svelte <-- Add the main page for the route here
```

## 2. Use `DefaultPage` or Other Page Component

Inside the `+page.svelte` file of the new folder (e.g., `new-app`), you can render the `DefaultPage` component to display content associated with a specific Firestore collection for that app. This is done by passing a constant from `INCA_APPS` as a prop.

```svelte
<script>
  import DefaultPage from "$lib/components/ui/DefaultPage.svelte";
  import { INCA_APPS } from "$lib/constants";

  // Use the corresponding constant for the app from INCA_APPS
  const firestoreCollection = INCA_APPS.NEW_APP; // Replace NEW_APP with the correct app
</script>

<DefaultPage {firestoreCollection} />
```

## 3. Modify `INCA_APPS` to Include New App

The `INCA_APPS` object in `src/lib/constants.js` exports the Firestore collection names dynamically based on environment variables. If your new app doesn't yet exist in `INCA_APPS`, you should add it and use an environment variable to manage the version. Here's an example of how the `INCA_APPS` constants are structured:

```js
export let INCA_APPS = {
  DELBOEUF: `Delboeuf-${import.meta.env.VITE_DELBOEUF_VERSION}`,
  WHICH_HAS_MORE: `Which-has-more-${
    import.meta.env.VITE_WHICH_HAS_MORE_VERSION
  }`,
  POP: `inca-pop-${import.meta.env.VITE_POP_VERSION}`,
  POP_END: `inca-pop-exit-end-${import.meta.env.VITE_POP_VERSION}`,
  CIO: `Click-in-Order-${import.meta.env.VITE_CIO_VERSION}`,
};
```

Each application has a unique version, which is dynamically read from environment variables such as `VITE_DELBOEUF_VERSION` or `VITE_WHICH_HAS_MORE_VERSION`. To add a new app, you should declare a new entry in `INCA_APPS` following the same format:

```js
NEW_APP: `new-app-${import.meta.env.VITE_NEW_APP_VERSION}`;
```

Ensure that the appropriate environment variable (e.g., `VITE_NEW_APP_VERSION`) is defined in your `.env` file.

## 4. Structure and Navigation

Ensure that the route for the new app (e.g., `/new-app`) is properly connected to other parts of the application. If there is a layout file defining shared navigation or page structure, ensure it includes a reference to your new route. The typical layout structure in `SvelteKit` is handled in a `+layout.svelte` file if needed.

For example, you might use the `TabNav` component in a `+layout.svelte` to create tab navigation across different pages:

```svelte
<script>
  import TabNav from "$lib/components/ui/TabNav.svelte";
  let activeTab = "data";
  function handleTabChange(tab) {
    activeTab = tab;
  }
</script>

<TabNav {activeTab} onChange={handleTabChange} />
```

## 5. Alternative to `DefaultPage`

If you're not using `DefaultPage` and want to use another page component, follow the same structure but replace `DefaultPage` with your custom component. Ensure that the component can handle the Firestore collection or other data required for the app.

Example:

```svelte
<script>
  import CustomPage from "$lib/components/ui/CustomPage.svelte";
  import { INCA_APPS } from "$lib/constants";

  const firestoreCollection = INCA_APPS.NEW_APP; // Replace with the new app constant
</script>

<CustomPage {firestoreCollection} />
```

## 6. Connect Firestore Data

Both `DefaultPage` and other custom components typically need to interact with Firestore to fetch and display data. Make sure your page component passes the necessary props (`firestoreCollection`, `userId`, etc.) for this integration.
