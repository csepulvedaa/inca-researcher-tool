---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "InCA - Researcher Tool"
  text: "InCA - Researcher Tool Documentation"
  tagline: Getting Started
  actions:
    - theme: brand
      text: Add An Application
      link: /add-an-app
    - theme: alt
      text: Enviroment Variables
      link: /api-examples
    - theme: alt
      text: API Examples
      link: /api-examples

features:
  - title: English 🇬🇧
    details: The InCA Researcher Tool is an advanced web application designed to facilitate the collection, management, and analysis of behavioral and cognitive data from digital life enrichment applications.
  - title: Spanish 🇪🇸
    details: La Herramienta InCA Researcher es una aplicación web avanzada diseñada para facilitar la recolección, gestión y análisis de datos conductuales y cognitivos provenientes de aplicaciones digitales de enriquecimiento de vida.
---
