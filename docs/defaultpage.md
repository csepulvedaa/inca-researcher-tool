# Documentation for `src/lib/components/ui/DefaultPage.svelte`

## Description

`DefaultPage.svelte` is the main interface component responsible for displaying user data within an InCA Or other app. It supports loading and displaying data through tabbed views such as a data browser, statistics, and charts. The component relies on user authentication to access the relevant Firestore collection and display content accordingly.

## Props

- **`firestoreCollection`**: The name of the Firestore collection from which the component fetches data.

## Reactive Variables

- **`learners`**: List of subjects available for selection.
- **`selectedSubject`**: The selected subject by the user.
- **`isLoggedIn`**: Boolean flag indicating if the user is logged in.
- **`showSelectSubjects`**: Boolean flag determining if the subject selection dropdown should be displayed.
- **`activeTab`**: The active tab in the UI, such as `"home"`, `"stats"`, or `"charts"`.
- **`userId`**: The authenticated user's ID.
- **`tempUserId`**: Temporary variable for manually entered user ID.
- **`loading`**: Boolean indicating if data is being loaded.
- **`actions`, `items`**: Data fetched from the Firestore collection.

## Lifecycle

- **`onMount`**: On component mount, it checks user authentication via `onAuthStateChanged`. If the user is authenticated, it fetches learners using `fetchLearners`, otherwise resets the state.

## Functions

1. **`resetState()`**: Resets all state variables if the user is not authenticated.
2. **`loadData()`**: Fetches data from Firestore based on the selected subject and `userId`, updating `items` accordingly.
3. **`handleUserIdSubmit()`**: Allows manual entry of `userId`, fetching learners and loading data based on the new ID.
4. **`handleTabChange(tab)`**: Changes the active tab view.

## User Interaction

- If logged in, users can select a subject and switch between different tabs (home, stats, charts) to view corresponding data.
- If not logged in, users are prompted to log in or manually submit a user ID.

## Components Used

- **`LearnerSelect`**: Dropdown for selecting a subject.
- **`TabNav`**: Tab navigation to switch between views.
- **`DataBrowser`**: Displays data in a table format.
- **`StatsDisplay`**: Shows data-related statistics.
- **`ChartsDisplay`**: Displays data in graphical format.
- **`fetchAndCacheFirestoreData`**: Function to fetch and cache Firestore data.

## Styling

Basic styles are included for structuring the page layout, centering elements such as the data table and charts, and managing the login screen container.

## Example Usage

This component is used as the main page interface for various INCA apps. Here's an example of how it can be used:

```svelte
<script>
  import DefaultPage from "$lib/components/ui/DefaultPage.svelte";
  import { INCA_APPS } from "$lib/constants";

  const firestoreCollection = INCA_APPS.WHICH_HAS_MORE;
</script>

<DefaultPage {firestoreCollection} />
```
