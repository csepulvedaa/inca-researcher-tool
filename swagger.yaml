openapi: 3.0.3
info:
  title: InCA - Backend Api
  version: 1.0.0
  description: >-
    ## **InCA - Backend Api**

    The **"InCA - Backend" API** provides a robust suite of statistical analysis
    tools designed to support research and data-driven insights in educational
    and psychological studies.


    This API allows users to query and retrieve statistical data related to
    various test results and experiments, facilitating deep analyses on subjects
    ranging from cognitive biases to decision-making processes. Each endpoint
    delivers specific statistical outputs, including display mode frequencies,
    value counts, results of binomial tests, contingency tables, and chi-squared
    tests, among others.

    ### **Key Features:**

    - **Display Modes Retrieval**: This endpoint offers detailed statistics on
    the frequency of various display modes used in experiments, segmented by
    different criteria such as user or subject area.

    - **Value Counts**: Users can access count and percentage data for
    responses, providing insights into correct versus incorrect answers within
    given datasets.

    - **Binomial Test**: This functionality allows researchers to perform
    binomial tests to determine if choices in a dataset were made at random or
    exhibit a pattern.

    - **Contingency Table**: Useful for analyzing the relationship between two
    categorical variables, this endpoint helps in preparing data for further
    statistical tests such as chi-squared tests.

    - **Chi-Squared Tests**: The API provides tools for conducting both Pearson
    Chi-Squared tests and tests for independence within contingency tables,
    crucial for hypothesis testing in research.

    - **Correlation Analysis**: Users can perform correlation tests, including
    Pearson correlation, to investigate the strength and direction of the linear
    relationship between two continuous variables.

    ### **Use Cases:**

    - **Educational Research**: Analyzing student responses to various test
    formats and educational tools.

    - **Psychological Studies**: Investigating decision-making processes and
    responses under different conditions.

    - **Market Research**: Understanding consumer preferences and behaviors
    through structured experiments.


    This API is designed for ease of use with comprehensive endpoints, detailed
    request parameters, and structured responses to support a wide range of
    statistical analyses. Its versatility makes it suitable for academics,
    researchers, and professionals needing detailed statistical evaluation and
    data exploration capabilities.  
servers:
  - url: http://backend.incalab.cl
paths:
  /api/statistics/display-modes:
    get:
      summary: Display Modes
      description: >-
        This endpoint retrieves the display modes statistics for a specific
        subject and user. It makes an HTTP GET request to the specified URL with
        query parameters including the subject, user ID, and the Firestore
        collection. The response returns a JSON object with the statistics for
        different display modes associated with the subject.


        The response body contains the statistics for each display mode, where
        the keys represent the display mode IDs and the values represent the
        counts for each mode.
      operationId: 0DisplayModes
      parameters:
        - name: subject
          in: query
          schema:
            type: string
            example: '{{subject}}'
        - name: uid
          in: query
          schema:
            type: string
            example: '{{uid}}'
        - name: firestoreCollection
          in: query
          schema:
            type: string
            example: '{{whichHasMoreCollection}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Value Counts:
                  value: |-
                    {
                        "2": {
                            "data": {
                                "correct": 222,
                                "incorrect": 110
                            },
                            "percentage": {
                                "correct": 66.87,
                                "incorrect": 33.13
                            }
                        },
                        "3": {
                            "data": {
                                "correct": 28,
                                "incorrect": 13
                            },
                            "percentage": {
                                "correct": 68.29,
                                "incorrect": 31.71
                            }
                        },
                        "4": {
                            "data": {
                                "correct": 10,
                                "incorrect": 14
                            },
                            "percentage": {
                                "correct": 41.67,
                                "incorrect": 58.33
                            }
                        },
                        "5": {
                            "data": {
                                "correct": 9,
                                "incorrect": 17
                            },
                            "percentage": {
                                "correct": 34.62,
                                "incorrect": 65.38
                            }
                        }
                    }
  /api/statistics/value-counts:
    get:
      summary: Value Counts
      description: >

        This endpoint makes an HTTP GET request to retrieve the value counts for
        a specific subject based on the provided parameters. The request URL
        should include the host, subject, uid, and firestoreCollection as query
        parameters. Upon successful execution, the response returns a status
        code of 200 and a JSON object containing value counts for different data
        points. Each data point includes the count of correct and incorrect
        values, along with the percentage of correctness.
      operationId: 1ValueCounts
      parameters:
        - name: subject
          in: query
          schema:
            type: string
            example: '{{subject}}'
        - name: uid
          in: query
          schema:
            type: string
            example: '{{uid}}'
        - name: firestoreCollection
          in: query
          schema:
            type: string
            example: '{{whichHasMoreCollection}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    {
                        "2": {
                            "data": {
                                "correct": 222,
                                "incorrect": 110
                            },
                            "percentage": {
                                "correct": 66.87,
                                "incorrect": 33.13
                            }
                        },
                        "3": {
                            "data": {
                                "correct": 28,
                                "incorrect": 13
                            },
                            "percentage": {
                                "correct": 68.29,
                                "incorrect": 31.71
                            }
                        },
                        "4": {
                            "data": {
                                "correct": 10,
                                "incorrect": 14
                            },
                            "percentage": {
                                "correct": 41.67,
                                "incorrect": 58.33
                            }
                        },
                        "5": {
                            "data": {
                                "correct": 9,
                                "incorrect": 17
                            },
                            "percentage": {
                                "correct": 34.62,
                                "incorrect": 65.38
                            }
                        }
                    }
  /api/statistics/binomial-test/:
    post:
      summary: Binomial Test
      description: >-
        This API endpoint is used to perform a binomial test for the given
        values. The HTTP POST request should be made to
        http://{{host}}/api/statistics/binomial-test/. The request body should
        be in raw format and contain the payload with the valueCounts.


        ### Request Body


        - valueCounts: (raw) The payload containing the values for which the
        binomial test needs to be performed.
            

        ### Response


        Upon a successful execution of the request, the API returns a JSON
        object with the status code 200 and the Content-Type as
        application/json. The response contains the pValue and notChoosedRandom
        for each value provided in the request payload.
      operationId: 2BinomialTest
      requestBody:
        content:
          application/json:
            examples:
              Binomial Test:
                value: '{{valueCounts}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    {
                        "2": {
                            "pValue": 7.772850789646423e-10,
                            "notChoosedRandom": true
                        },
                        "3": {
                            "pValue": 0.027533155829587486,
                            "notChoosedRandom": true
                        },
                        "4": {
                            "pValue": 0.5412561893463135,
                            "notChoosedRandom": false
                        },
                        "5": {
                            "pValue": 0.16863754391670227,
                            "notChoosedRandom": false
                        }
                    }
  /api/statistics/contingency-table/:
    get:
      summary: Contingency Table
      description: >-
        This API endpoint retrieves the contingency table statistics based on
        the provided parameters.


        ### Request Parameters


        - `subject` (string): The subject for which the contingency table
        statistics are requested.

        - `uid` (string): The unique identifier associated with the subject.

        - `firestoreCollection` (string): The name of the Firestore collection
        containing the data.
            

        ### Response


        Upon a successful execution, the API returns a JSON object with the
        contingency table statistics. The structure of the response object is as
        follows:


        The keys "True" and "False" represent the boolean values, and each
        contains the count of occurrences for "right", "left", and "unknown"
        categories.
      operationId: 3ContingencyTable
      parameters:
        - name: subject
          in: query
          schema:
            type: string
            example: '{{subject}}'
        - name: uid
          in: query
          schema:
            type: string
            example: '{{uid}}'
        - name: firestoreCollection
          in: query
          schema:
            type: string
            example: '{{whichHasMoreCollection}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    {
                        "True": {
                            "right": 95,
                            "left": 127,
                            "unknown": 47
                        },
                        "False": {
                            "right": 43,
                            "left": 67,
                            "unknown": 44
                        }
                    }
  /api/statistics/chi-squared/contingency:
    post:
      summary: Chi Squared Contingency
      description: >

        This HTTP POST request is used to calculate the chi-squared statistic
        for a contingency table. The request should be sent to
        http://{{host}}/api/statistics/chi-squared/contingency.


        ### Request Body

        The request body should be of raw type, and the payload should contain
        the contingency table data in the format specified by the API
        documentation.


        ### Response

        Upon successful execution, the API returns a JSON object with the
        following fields:

        - `chiSquared`: The calculated chi-squared statistic.

        - `pValue`: The p-value associated with the chi-squared statistic.

        - `degreesOfFreedom`: The degrees of freedom for the chi-squared test.

        - `lessThanSignificance`: A boolean indicating whether the chi-squared
        test is significant.

        - `colTotals`: An object containing the total counts for each column in
        the contingency table.

        - `rowTotals`: An object containing the total counts for each row in the
        contingency table.
      operationId: 4ChiSquaredContingency
      requestBody:
        content:
          application/json:
            examples:
              4.- Chi Squared Contingency:
                value: '{{contingencyTable}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    {
                        "chiSquared": 7.542511731297636,
                        "pValue": 0.05214062236984707,
                        "degreesOfFreedom": 2,
                        "lessThanSignificance": false,
                        "colTotals": {
                            "right": 138,
                            "left": 194,
                            "unknown": 91
                        },
                        "rowTotals": {
                            "True": 269,
                            "False": 154
                        }
                    }
  /api/statistics/chi-squared:
    post:
      summary: Chi Squared Pearson
      description: >

        This endpoint allows you to calculate the chi-squared statistic for a
        given contingency table. 


        ### Request Body

        - `contingencyTable` (raw): The contingency table data for which the
        chi-squared statistic needs to be calculated.


        ### Response

        Upon a successful execution, the response will include the following
        fields:

        - `chiSquared`: The calculated chi-squared statistic.

        - `pValue`: The p-value associated with the chi-squared statistic.

        - `degreesOfFreedom`: The degrees of freedom for the chi-squared test.

        - `lessThanSignificance`: A boolean indicating if the chi-squared
        statistic is less than the significance level.

        - `totals`: An object containing the total counts for the different
        categories.
      operationId: 5ChiSquaredPearson
      requestBody:
        content:
          application/json:
            examples:
              Chi Squared Pearson:
                value: '{{contingencyTable}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    {
                        "chiSquared": 9.44578313253012,
                        "pValue": 0.0021163520290568005,
                        "degreesOfFreedom": 1,
                        "lessThanSignificance": true,
                        "totals": {
                            "right": 138,
                            "left": 194
                        }
                    }
  /api/statistics/correlation/values:
    get:
      summary: Correlation Test
      description: >-
        This API endpoint retrieves correlation statistics values based on the
        provided subject, uid, and firestore collection. The request should be a
        GET method to the specified URL with the required query parameters. Upon
        successful execution, the API returns a JSON array containing the
        correlation statistics values such as Value 1, Value 2, Total,
        Difference, Ratio, Accuracy, and Total Experiments.
      operationId: 6CorrelationTest
      parameters:
        - name: subject
          in: query
          schema:
            type: string
            example: '{{subject}}'
        - name: uid
          in: query
          schema:
            type: string
            example: '{{uid}}'
        - name: firestoreCollection
          in: query
          schema:
            type: string
            example: '{{whichHasMoreCollection}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    [
                        {
                            "Value 1": 4,
                            "Value 2": 6,
                            "Total": 10,
                            "Difference": 2,
                            "Ratio": "0.67",
                            "Accuracy": "42.86",
                            "Total Experiments": 21
                        },
                        {
                            "Value 1": 4,
                            "Value 2": 3,
                            "Total": 7,
                            "Difference": 1,
                            "Ratio": "0.75",
                            "Accuracy": "66.67",
                            "Total Experiments": 27
                        },
                        {
                            "Value 1": 4,
                            "Value 2": 5,
                            "Total": 9,
                            "Difference": 1,
                            "Ratio": "0.80",
                            "Accuracy": "65.22",
                            "Total Experiments": 23
                        },
                        {
                            "Value 1": 4,
                            "Value 2": 1,
                            "Total": 5,
                            "Difference": 3,
                            "Ratio": "0.25",
                            "Accuracy": "88.89",
                            "Total Experiments": 18
                        },
                        {
                            "Value 1": 4,
                            "Value 2": 2,
                            "Total": 6,
                            "Difference": 2,
                            "Ratio": "0.50",
                            "Accuracy": "64.00",
                            "Total Experiments": 25
                        },
                        {
                            "Value 1": 6,
                            "Value 2": 3,
                            "Total": 9,
                            "Difference": 3,
                            "Ratio": "0.50",
                            "Accuracy": "68.75",
                            "Total Experiments": 16
                        },
                        {
                            "Value 1": 6,
                            "Value 2": 5,
                            "Total": 11,
                            "Difference": 1,
                            "Ratio": "0.83",
                            "Accuracy": "50.00",
                            "Total Experiments": 18
                        },
                        {
                            "Value 1": 6,
                            "Value 2": 1,
                            "Total": 7,
                            "Difference": 5,
                            "Ratio": "0.17",
                            "Accuracy": "80.00",
                            "Total Experiments": 20
                        },
                        {
                            "Value 1": 6,
                            "Value 2": 2,
                            "Total": 8,
                            "Difference": 4,
                            "Ratio": "0.33",
                            "Accuracy": "72.73",
                            "Total Experiments": 33
                        },
                        {
                            "Value 1": 3,
                            "Value 2": 5,
                            "Total": 8,
                            "Difference": 2,
                            "Ratio": "0.60",
                            "Accuracy": "55.56",
                            "Total Experiments": 18
                        },
                        {
                            "Value 1": 3,
                            "Value 2": 1,
                            "Total": 4,
                            "Difference": 2,
                            "Ratio": "0.33",
                            "Accuracy": "75.86",
                            "Total Experiments": 29
                        },
                        {
                            "Value 1": 3,
                            "Value 2": 2,
                            "Total": 5,
                            "Difference": 1,
                            "Ratio": "0.67",
                            "Accuracy": "71.43",
                            "Total Experiments": 21
                        },
                        {
                            "Value 1": 5,
                            "Value 2": 1,
                            "Total": 6,
                            "Difference": 4,
                            "Ratio": "0.20",
                            "Accuracy": "70.00",
                            "Total Experiments": 20
                        },
                        {
                            "Value 1": 5,
                            "Value 2": 2,
                            "Total": 7,
                            "Difference": 3,
                            "Ratio": "0.40",
                            "Accuracy": "71.43",
                            "Total Experiments": 21
                        },
                        {
                            "Value 1": 1,
                            "Value 2": 2,
                            "Total": 3,
                            "Difference": 1,
                            "Ratio": "0.50",
                            "Accuracy": "54.55",
                            "Total Experiments": 22
                        }
                    ]
  /api/statistics/correlation/pearson:
    post:
      summary: Correlation Pearson
      description: >-


        This API endpoint allows you to calculate the Pearson correlation
        coefficient for the provided values.




        - **correlationValues** (raw text): The values for which the correlation
        coefficient needs to be calculated.
            

        ### Response


        Upon a successful execution, the API returns a JSON object with the
        following structure:

        - The response provides the correlation statistics for the provided
        values under different categories.
      operationId: 7CorrelationPearson
      requestBody:
        content:
          application/json:
            examples:
              Correlation Pearson:
                value: '{{correlationValues}}'
      responses:
        '200':
          description: ''
          content:
            application/json:
              examples:
                Example Response:
                  value: |-
                    {
                        "Total": {
                            "Total": 1,
                            "Difference": 0,
                            "Ratio": 0.49051275206411,
                            "Accuracy": -0.4720123406557
                        },
                        "Difference": {
                            "Total": 0,
                            "Difference": 1,
                            "Ratio": -0.84959300833548,
                            "Accuracy": 0.53077160786111
                        },
                        "Ratio": {
                            "Total": 0.49051275206411,
                            "Difference": -0.84959300833548,
                            "Ratio": 1,
                            "Accuracy": -0.67365816863012
                        },
                        "Accuracy": {
                            "Total": -0.4720123406557,
                            "Difference": 0.53077160786111,
                            "Ratio": -0.67365816863012,
                            "Accuracy": 1
                        }
                    }
tags: []
